﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.DTO
{
    public class TopUpVehicleDataDTO
    {
        [JsonProperty("id")]
        [RegularExpression(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$")]
        [Required]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        
        [Required]
        public decimal Sum { get; set; }
    }
}
