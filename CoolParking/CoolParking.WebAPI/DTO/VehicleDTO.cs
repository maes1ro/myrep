﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.DTO
{
    public class VehicleDTO
    {
        [JsonProperty("id")]
        [Required]
        [RegularExpression(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        [Required]
        public VehicleType VehicleType { get; set; }
        [JsonProperty("balance")]
        [Required]
        public decimal Balance { get; set; }
    }
}
