﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using System.Text.RegularExpressions;
using CoolParking.WebAPI.DTO;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : Controller
    {
        IParkingService parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet]
        public IActionResult Vehicles()
        {
            return Ok(parkingService.GetVehicles());
        }
        [HttpGet("{id}")]
        public IActionResult VehiclesById(string id)
        {
            if (!Vehicle.IsValidId(id))
            {
               return StatusCode(400, "Invalid id");
            }
            else if (parkingService.GetVehicleById(id) == null)
            {
                return StatusCode(404, "Vehicle not found");
            }
            else
            {
                return Ok(parkingService.GetVehicleById(id));
            }
        }
        [HttpPost]
        public IActionResult PostVehicle([FromBody]VehicleDTO vehicle)
        {
            if (ModelState.IsValid && ((int)vehicle.VehicleType >= 1 && (int)vehicle.VehicleType <= 4))
            {
                try
                {
                    var item = new Vehicle(vehicle.Id, vehicle.VehicleType, vehicle.Balance);
                    parkingService.AddVehicle(item);
                    return StatusCode(201, item);
                }
                catch (ArgumentException)
                {
                    return StatusCode(400, "Body is invalid");
                }
            }
            else
            {
                return StatusCode(400, "Body is invalid");
            }
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicleById(string id)
        {
            if (!Vehicle.IsValidId(id))
            {
                return StatusCode(400, "Invalid id");
            }
            else if (parkingService.GetVehicleById(id) == null)
            {
                return StatusCode(404, "Vehicle not found");
            }
            else
            {
                parkingService.RemoveVehicle(id);
                return StatusCode(204);
            }
        }
    }
}
