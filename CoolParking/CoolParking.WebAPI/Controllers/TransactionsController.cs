﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.DTO;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        IParkingService parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        [HttpGet("last")]
        public IActionResult Last()
        {
            return Ok(parkingService.GetLastParkingTransactions());

        }
        [HttpGet("all")]
        public IActionResult All()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                return StatusCode(404, "File not found");
            }
        }
        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody]TopUpVehicleDataDTO data)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(400, "Invalid body");
            }
            else if (parkingService.GetVehicleById(data.Id) == null)
            {
                return StatusCode(404, "Vehicle not found");
            }
            else
            {
                try
                {
                    parkingService.TopUpVehicle(data.Id, data.Sum);
                    return Ok(parkingService.GetVehicleById(data.Id));
                }
                catch (ArgumentException)
                {
                    return StatusCode(400, "Invalid body");
                }
                
            }
        }
    }
}
