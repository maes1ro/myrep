﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : Controller
    {
        IParkingService parkingService;

        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        [HttpGet("balance")]
        public ActionResult Balance()
        {
            return Ok(parkingService.GetBalance());
        }
        [HttpGet("capacity")]
        public ActionResult Capacity()
        {
            return Ok(parkingService.GetCapacity());
        }
        [HttpGet("freePlaces")]
        public ActionResult FreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }
    }
}
