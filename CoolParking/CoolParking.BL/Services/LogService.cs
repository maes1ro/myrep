﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        List<TransactionInfo> transList = new List<TransactionInfo>();
        public List<TransactionInfo> TransactionInfo
        {
            get => transList;
            set => transList = value;
        } 
        public string LogPath { get; }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public void Write(string logInfo)
        {
            if (!File.Exists(LogPath))
            {
                File.Create(LogPath).Close();
            }
            using (StreamWriter writer = new StreamWriter(LogPath, true))
            {
                writer.WriteLine(logInfo);
            }
        }
        public string Read()
        {
            string logInfo = string.Empty;
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException();
            }
            using (StreamReader reader = new StreamReader(LogPath,true))
            {
                logInfo = reader.ReadToEnd();
            }
            return logInfo;
        }
    }
}