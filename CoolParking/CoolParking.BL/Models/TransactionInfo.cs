﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonProperty("vehicleId")]
        [Required]
        [RegularExpression(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$")]
        public string VehicleId { get; set; }
        [JsonProperty("sum")]
        [Required]
        public decimal Sum { get; set; }
        [JsonProperty("transactionDate")]
        [Required]
        public DateTime TransactionDate { get; set; }
    }
}
