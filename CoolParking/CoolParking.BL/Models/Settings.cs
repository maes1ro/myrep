﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.ObjectModel;
using System.Collections.Generic;
namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal ParkingStartBalance { get; } = 0;
        public static int Capacity { get; } = 10;
        public static int ChangeOffPeriod { get; } = 5000;
        public static int LogWritePeriod { get; } = 600000;
        public static double PenaltyCoefficient { get; } = 2.5;
        private static ReadOnlyDictionary<VehicleType,decimal> tariffs = new ReadOnlyDictionary<VehicleType, decimal>(new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.PassengerCar, 2M },
            { VehicleType.Truck, 5M },
            { VehicleType.Bus, 3.5M },
            { VehicleType.Motorcycle, 1M }
        }) ;
        public static decimal Tariffs(VehicleType type)
        {
            switch (type)
            {
                case VehicleType.PassengerCar:
                    return tariffs[VehicleType.PassengerCar];
                case VehicleType.Truck:
                    return tariffs[VehicleType.Truck];
                case VehicleType.Bus:
                    return tariffs[VehicleType.Bus];
                case VehicleType.Motorcycle:
                    return tariffs[VehicleType.Motorcycle];
                default: return 0;
            }
            

        }
    }
}