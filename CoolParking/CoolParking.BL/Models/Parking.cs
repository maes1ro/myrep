﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking parking;
        private List<Vehicle> vehicles;

        public decimal Balance { get; set; } = Settings.ParkingStartBalance;

        public List<Vehicle> Vehicles
        {
            get
            {
                return vehicles;
            }
        }
        private Parking() { vehicles = new List<Vehicle>() { Capacity = Settings.Capacity }; }

        public static Parking GetInstance()
        {
            if (parking == null)
            {
                parking = new Parking();
                return parking;
            }
            else
            {
                return parking;
            }
        }
        public void WithdrawSum(string vehicleId, decimal sum)
        {
            var vehicle = Vehicles.Find(v => v.Id == vehicleId);
            if (vehicle != null)
            {
                vehicle.Balance -= sum;
                Balance += sum;
            }
        }
        public void Dispose()
        {
            Vehicles.Clear();
            Balance = 0;
        }
    }
}
