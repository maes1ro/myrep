﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.WebAPI.DTO;
using CoolParking.BL.Models;

namespace Gui.Infrastructure
{
    static class UserMessageFormat
    {
        public static string StringFormat(this object data)
        {
            return data as string;
        }

        public static string VehicleDtoStringFormat(this object data, string message="")
        {
            if (data is VehicleDTO)
            {
                return $"{message}{(data as VehicleDTO).Id} - {(data as VehicleDTO).VehicleType} - {(data as VehicleDTO).Balance}";
            }
            else
            {
                return data as string;
            }
        }
        public static string TransacionInfoStringFormat(this object data, string message="")
        {
            if (data is TransactionInfo)
            {
                return $"{message}{((TransactionInfo)data).VehicleId} - {((TransactionInfo)data).Sum} - {((TransactionInfo)data).TransactionDate}";
            }
            else
            {
                return data as string;
            }
        }
    }
}
