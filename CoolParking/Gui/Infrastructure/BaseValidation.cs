﻿using System;

namespace Gui.Infrastructure
{
    static class BaseValidation
    {
        public static int IntParse()
        {
            int result;
            while (!int.TryParse(Console.ReadLine(), out result))
            {
                Console.WriteLine("Input not integer, try again");
            }
            return result;
        }

        public static decimal DecimalParse()
        {
            decimal result;
            while (!decimal.TryParse(Console.ReadLine(), out result))
            {
                Console.WriteLine("Input not a decimal, try again");
            }
            return result;
        }
    }
}
