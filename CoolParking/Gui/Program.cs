﻿using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using System.Threading.Tasks;
using CoolParking.WebAPI.DTO;
using Gui.Interface;
using Gui.Service;
using Gui.Infrastructure;

namespace UserInterface
{
    class Gui
    {
        static async Task Main(string[] args)
        {
            string baseAddress = "https://localhost:44316/api/";
            IRequestService service = new RequestService(baseAddress);
            do
            {
                Console.WriteLine("\t\tMenu\n\t - Parking balance - 1\n\t " +
                  "- Parking capacity - 2\n\t " +
                  "- Free places - 3\n\t " +
                  "- Vehicles at the parking - 4\n\t " +
                  "- Get vehicle - 5\n\t " +
                  "- Put vehicle at the parking - 6\n\t " +
                  "- Remove vehicle from the parking - 7\n\t " +
                  "- Last transactions - 8\n\t " +
                  "- Transactions history - 9\n\t " +
                  "- Top up vehicle balance - 10\n\t " +
                  " Or write exit to out");

                var input = Console.ReadLine();
                if (!new Regex(@"^([1-9]|10|([e|E][x|X][i|I][t|T]))$").IsMatch(input))
                {
                    Console.WriteLine("No such option");
                }
                else
                {
                    if (input.ToLower() == "exit")
                    {
                        break;
                    }
                    switch (int.Parse(input))
                    {
                        case 1:
                            Console.WriteLine("Parking balance is " + (await service.GetParkingBalance()).ToString());
                            break;
                        case 2:
                            Console.WriteLine("Parking capacity is " + (await service.GetParkingCapacity()).ToString());
                            break;
                        case 3:
                            Console.WriteLine("Free places: " + (await service.GetParkingFreePlaces()).ToString());
                            break;
                        case 4:
                            Console.WriteLine("Vehicles at the parking: ");

                            var vehicles = await service.GetVehicles();
                            foreach (var i in vehicles)
                            {
                                Console.WriteLine(i.VehicleDtoStringFormat());
                            }
                            break;
                        case 5:
                            Console.WriteLine("Enter id of car:");
                            string id = Console.ReadLine();

                            var vehicle = await service.GetVehicleById(id);

                            Console.WriteLine($"{vehicle.VehicleDtoStringFormat("Information about your vehicle: ")}");
                            break;
                        case 6:
                            Console.WriteLine("Enter car id: ");
                            string Id = Console.ReadLine();

                            Console.WriteLine("Choose type of vehicle:\n" + 
                                               "Passenger car - 1\n" + 
                                               "Truck  - 2\n" +
                                               "Bus - 3\n" + 
                                               "Motorcycle - 4\n");
                            int vehicleType = BaseValidation.IntParse();

                            Console.WriteLine("Enter balance: ");
                            decimal balance = BaseValidation.DecimalParse();

                            var newVehicle = new VehicleDTO() { Id = Id, VehicleType = (VehicleType)vehicleType, Balance = balance };

                            var result = await service.PostVehicle(newVehicle);
                            Console.WriteLine($"{result.VehicleDtoStringFormat("Created vehicle: ")}");
                            break;
                        case 7:
                            Console.WriteLine("Enter car id:");
                            string carId = Console.ReadLine();

                            var deletedVehicle = await service.DeleteVehicle(carId);
                            Console.WriteLine($"{deletedVehicle.StringFormat()}");
                            break;
                        case 8:
                            Console.WriteLine("Last transactions: ");

                            var transactions = await service.GetTransatctionsLast();
                            foreach (var i in transactions)
                            {
                                Console.WriteLine($"{i.TransacionInfoStringFormat()}");
                            }
                            break;
                        case 9:
                            Console.WriteLine("Transaction history:");
                            Console.WriteLine(await service.GetTransatctionsAll());
                            break;
                        case 10:
                            Console.WriteLine("Enter vehicle id: ");
                            string vehicleId = Console.ReadLine();

                            Console.WriteLine("Enter sum to add: ");
                            decimal sum = BaseValidation.DecimalParse();

                            var topUpVehicle = new TopUpVehicleDataDTO() { Id = vehicleId, Sum = sum };
                            var vehicleAfterTopUp = await service.PutTransatctionsTopUpVehicle(topUpVehicle);

                            Console.WriteLine($"{vehicleAfterTopUp.VehicleDtoStringFormat("Vehicle after top up balance: ")}");
                            break;
                    }
                }
            } while (true);
        }
    }
}