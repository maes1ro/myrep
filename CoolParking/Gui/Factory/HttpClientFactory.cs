﻿using System;
using System.Net.Http;

namespace Gui.Factory
{
    internal static class HttpClientFactory
    {
        internal static HttpClient Create(string baseAddress)
        {
            return new HttpClient() { BaseAddress = new Uri(baseAddress) };
        }
    }
}
