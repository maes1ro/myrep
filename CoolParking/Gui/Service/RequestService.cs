﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Gui.Interface;
using System.Net.Http;
using Gui.Factory;
using Newtonsoft.Json;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTO;

namespace Gui.Service
{
    class RequestService : IRequestService
    {
        readonly HttpClient client;

        public RequestService(string baseAddress)
        {
            client = HttpClientFactory.Create(baseAddress);
        }

        public async Task<decimal> GetParkingBalance()
        {
            var balance = await client.GetAsync("parking/balance");
            var response = await balance.Content.ReadAsStringAsync();
            return decimal.Parse(response);
        }

        public  async Task<int> GetParkingCapacity()
        {
            var capacity = await client.GetAsync("parking/capacity");
            var response = await capacity.Content.ReadAsStringAsync();
            return int.Parse(response);
        }

        public  async Task<int> GetParkingFreePlaces()
        {
            var freePlaces = await client.GetAsync("parking/freePlaces");
            var response = await freePlaces.Content.ReadAsStringAsync();
            return int.Parse(response);
        }

        public  async Task<List<VehicleDTO>> GetVehicles()
        {
            var vehicles = await client.GetAsync("vehicles");
            var response = await vehicles.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<VehicleDTO>>(response);
        }

        public  async Task<object> GetVehicleById( string id)
        {
            var vehicle = await client.GetAsync($"vehicles/{id}");
            try
            {
                var response = await vehicle.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VehicleDTO>(response);
            }
            catch (HttpRequestException)
            {
                return await vehicle.Content.ReadAsStringAsync();
            }
            
        }

        public async Task<object> PostVehicle( VehicleDTO vehicle)
        {
            var json = JsonConvert.SerializeObject(vehicle);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var created = await client.PostAsync("vehicles", stringContent);
            try
            {
                var createdContent = await created.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VehicleDTO>(createdContent);
            }
            catch (HttpRequestException)
            {
                return await created.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> DeleteVehicle(string id)
        {
            var a = await client.DeleteAsync($"vehicles/{id}");
            try
            {
                a.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException)
            {
                return await a.Content.ReadAsStringAsync();
            }
            return "Success";
        }

        public async Task<List<TransactionInfo>> GetTransatctionsLast()
        {
            var last = await client.GetAsync("transactions/last");
            var response = await last.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<TransactionInfo>>(response);
        }

        public async Task<string> GetTransatctionsAll()
        {
            var all = await client.GetAsync("transactions/all");
            try
            {
                return await all.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
            }
            catch (HttpRequestException)
            {
                return await all.Content.ReadAsStringAsync();
            }
        }

        public async Task<object> PutTransatctionsTopUpVehicle(TopUpVehicleDataDTO data)
        {
            var json = JsonConvert.SerializeObject(data);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var vehicle = await client.PutAsync("transactions/topUpVehicle", stringContent);
            try
            {
                var response = await vehicle.EnsureSuccessStatusCode().Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VehicleDTO>(response);
            }
            catch (System.Exception)
            {
                return await vehicle.Content.ReadAsStringAsync();
            }
        }

    }
}
