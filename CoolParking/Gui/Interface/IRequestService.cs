﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using CoolParking.WebAPI.DTO;
using CoolParking.BL.Models;

namespace Gui.Interface
{
    interface IRequestService
    {
        Task<decimal> GetParkingBalance();
        Task<int> GetParkingCapacity();
        Task<int> GetParkingFreePlaces();
        Task<List<VehicleDTO>> GetVehicles();
        Task<object> GetVehicleById(string id);
        Task<object> PostVehicle(VehicleDTO vehicle);
        Task<string> DeleteVehicle(string id);
        Task<List<TransactionInfo>> GetTransatctionsLast();
        Task<string> GetTransatctionsAll();
        Task<object> PutTransatctionsTopUpVehicle(TopUpVehicleDataDTO data);


    }
}
